<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransNilaiAlternatifKmeans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_nilai_alternatif_kmeans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('alternatif_id')->unsigned()->default(1);
            $table->foreign('alternatif_id')->references('id')->on('ref_alternatif');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('trans_nilai_alternatif_kmeans_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('nilai_id')->unsigned()->default(1);
            $table->integer('bantuan_id')->unsigned()->default(1);
            $table->integer('nilai');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();

            $table->foreign('nilai_id')->references('id')->on('trans_nilai_alternatif_kmeans');
            $table->foreign('bantuan_id')->references('id')->on('ref_bantuan');
        });

        Schema::create('log_trans_nilai_alternatif_kmeans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('alternatif_id')->unsigned()->default(1);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_trans_nilai_alternatif_kmeans_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('nilai_id')->unsigned()->default(1);
            $table->integer('bantuan_id')->unsigned()->default(1);
            $table->integer('nilai');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_trans_nilai_alternatif_kmeans_detail');
        Schema::dropIfExists('log_trans_nilai_alternatif_kmeans');
        Schema::dropIfExists('trans_nilai_alternatif_kmeans_detail');
        Schema::dropIfExists('trans_nilai_alternatif_kmeans');
    }
}
