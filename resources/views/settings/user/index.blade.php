@extends('layouts.list-user')

@section('title', 'Manajemen Pengguna')

@section('side-header')
    <div class="btn-group" style="left: -15px;">
        <a href="{{ route('setting.users.index') }}" type="button" class="btn btn-grey ada active" style="border-radius: 20px;">User</a>
        <a href="{{ route('setting.roles.index') }}" type="button" class="btn btn-grey ada" style="border-radius: 20px;">Role</a>
        <a href="{{ route('setting.log.index') }}" type="button" class="btn btn-grey hilang" style="border-radius: 20px;">Log</a>
    </div>
@endsection

@section('filters')
    <div class="form-group">
        <label class="control-label m-r-sm" for="filters"><i class="fa fa-filter"></i> &nbsp; Filter</label>
        <label class="control-label sr-only" for="filter-name">Nama</label>
        <input type="text" class="form-control filter-control" name="filter[name]" data-post="name" placeholder="Nama">
    </div>
    <div class="form-group m-l-sm">
        <label class="control-label sr-only" for="filter-email">E-Mail</label>
        <input type="text" class="form-control filter-control" name="filter[email]" data-post="email" placeholder="Email">
    </div>
    <label class="control-label sr-only" for="filter-role">Role</label>
    <select class="select selectpicker filter-control show-tick" name="filter[role]" data-style="btn-default" data-post="role" title="Role" data-size="5" data-live-search="true">
    	<option style="font-size: 12px;" value="">(Pilih Salah Satu)</option>
        @foreach(Spatie\Permission\Models\Role::get() as $role)
            <option value="{{ $role->id }}">{{ $role->name }}</option>
        @endforeach
    </select>
    <label class="control-label sr-only" for="filter-divisi">Divisi</label>
    <select class="select selectpicker filter-control show-tick" name="filter[divisi]" data-style="btn-default" data-post="divisi" title="Divisi" data-size="5" data-live-search="true">
        <option style="font-size: 12px;" value="">(Pilih Salah Satu)</option>
        @foreach(App\Models\Master\BU::orderBy('nama', 'asc')->get() as $divisi)
            <option value="{{ 'bu-'.$divisi->id }}">BU - {{ $divisi->nama }}</option>
        @endforeach
        @foreach(App\Models\Master\CO::orderBy('nama', 'asc')->get() as $divisi)
            <option value="{{ 'co-'.$divisi->id }}">CO - {{ $divisi->nama }}</option>
        @endforeach
    </select>
    <label class="control-label sr-only" for="filter-fungsi">Fungsional</label>
    <select class="select selectpicker filter-control" name="filter[fungsi]" data-post="fungsi" data-style="btn-default" data-size="5" > 
           <option style="font-size: 12px;" value="">Fungsional</option>
           <option style="font-size: 12px;" value="1">Operasional</option>
           <option style="font-size: 12px;" value="2">Keuangan</option>
           <option style="font-size: 12px;" value="3">Sistem</option>
    </select>
@endsection