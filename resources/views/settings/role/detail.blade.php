@extends('layouts.base')

@include('libs.actions')

@section('title', 'Manajemen Pengguna')

@section('side-header')
    <div class="btn-group" style="left: -15px;">
        <a href="{{ route('setting.users.index') }}" type="button" class="btn btn-default" style="border-radius: 20px;">User</a>
        <a href="{{ route('setting.roles.index') }}" type="button" class="btn btn-violet" style="border-radius: 20px;">Role</a>
    </div>
@endsection

@section('body')
	<form id="formData" action="{{ route($routes.'.update', $record->id) }}" method="POST">
		@method('PATCH')
		@csrf
		
		<input type="hidden" name="name" value="{{ $record->name }}">
		<input type="hidden" name="id" value="{{ $record->id }}">
		<div class="panel panel-default" style="font-size: 12px;">
			<div class="panel-heading" style="background-color: #0f2233;">
				<h4 class="font-thin h4">
					<span style="color: white;font-size: 16px;font-weight: bold;">Role : {{ $record->name }}</span>
					<span class="pull-right" style="color: white;font-size: 16px;font-weight: bold;">{{ $record->users->count() }} Users</span>
				</h4>
			</div>
			<table class="table table-striped b-t b-light">
				<thead>
					<tr>
						<th><i class="fa fa-dedent fa-fw text"></i>&nbsp;&nbsp;&nbsp;Menu</th>
						<th class="text-center">View</th>
						<th class="text-center">Create</th>
						<th class="text-center">Update</th>
						<th class="text-center">Delete</th>
						<th class="text-center"></th>
					</tr>
				</thead>
				@foreach($sideMenu->roots() as $item)
				@php
					if(is_array($item->perms))
					{
						$perms = \App\Models\Auths\Permission::where(function($query) use ($item) {
								for($i = 0; $i < count($item->perms); $i++)
								{
										$query->orWhere('name', 'like', $item->perms[$i].'%');
								}
						})->get();
					}else{
						$perms = \App\Models\Auths\Permission::where('name', 'like', $item->perms.'%')->get();
					}
				@endphp
					<thead>
						<tr style="background-color: #f0f0f0;">
							<th><i class="{{ $item->data['icon'] }} icon"></i>&nbsp;&nbsp;&nbsp;{{ $item->title}}</th>
							<th class="text-center">
								@if($item->hasChildren())
									<button type="button" class="btn btn-xs btn-default vertical all" style="border-radius: 20px;" data-action="view">&nbsp;&nbsp;<i class="fa fa-check"></i>&nbsp;&nbsp; View All&nbsp;&nbsp;&nbsp;</button>
								@else
									@if($p = $perms->where('name', $item->perms.'-view')->first())
										<input type="checkbox" class="view check" name="check[]" value="{{ $p->name }}" @if($record->hasPermissionTo($item->perms.'-view')) checked @endif data-toggle="toggle" data-size="mini" data-on="Ya" data-off="Tidak" data-style="ios">
									@else
										<input type="checkbox" class="view check" name="check[]" value="{{ $item->perms.'-view' }}" data-toggle="toggle" data-size="mini" data-on="Ya" data-off="Tidak" data-style="ios">
									@endif
								@endif
							</th>
							<th class="text-center">
								@if($item->hasChildren())
									<button type="button" class="btn btn-xs btn-default vertical all" style="border-radius: 20px;" data-action="create">&nbsp;&nbsp;<i class="fa fa-check"></i>&nbsp;&nbsp; Create All&nbsp;&nbsp;&nbsp;</button>
								@else
									@if($p = $perms->where('name', $item->perms.'-add')->first())
										<input type="checkbox" class="create check" name="check[]" value="{{ $p->name }}" @if($record->hasPermissionTo($item->perms.'-add')) checked @endif data-toggle="toggle" data-size="mini" data-on="Ya" data-off="Tidak" data-style="ios">
									@else
										<input type="checkbox" class="create check" name="check[]" value="{{ $item->perms.'-add' }}" data-toggle="toggle" data-size="mini" data-on="Ya" data-off="Tidak" data-style="ios">
									@endif
								@endif
							</th>
							<th class="text-center">
								@if($item->hasChildren())
									<button type="button" class="btn btn-xs btn-default vertical all" style="border-radius: 20px;" data-action="update">&nbsp;&nbsp;<i class="fa fa-check"></i>&nbsp;&nbsp; Update All&nbsp;&nbsp;&nbsp;</button>
								@else
									@if($p = $perms->where('name', $item->perms.'-edit')->first())
										<input type="checkbox" class="update check" name="check[]" value="{{ $p->name }}" @if($record->hasPermissionTo($item->perms.'-edit')) checked @endif data-toggle="toggle" data-size="mini" data-on="Ya" data-off="Tidak" data-style="ios">
									@else
										<input type="checkbox" class="update check" name="check[]" value="{{ $item->perms.'-edit' }}" data-toggle="toggle" data-size="mini" data-on="Ya" data-off="Tidak" data-style="ios">
									@endif
								@endif
							</th>
							<th class="text-center">
								@if($item->hasChildren())
									<button type="button" class="btn btn-xs btn-default vertical all" style="border-radius: 20px;" data-action="delete">&nbsp;&nbsp;<i class="fa fa-check"></i>&nbsp;&nbsp; Delete All&nbsp;&nbsp;&nbsp;</button>
								@else
									@if($p = $perms->where('name', $item->perms.'-delete')->first())
										<input type="checkbox" class="delete check" name="check[]" value="{{ $p->name }}" @if($record->hasPermissionTo($item->perms.'-delete')) checked @endif data-toggle="toggle" data-size="mini" data-on="Ya" data-off="Tidak" data-style="ios">
									@else
										<input type="checkbox" class="delete check" name="check[]" value="{{ $item->perms.'-delete' }}" data-toggle="toggle" data-size="mini" data-on="Ya" data-off="Tidak" data-style="ios">
									@endif
								@endif
							</th>
							<th class="text-center">
								@if($item->hasChildren())
								@else
									<button type="button" class="btn btn-xs btn-default horizontal all" style="border-radius: 20px;">&nbsp;&nbsp;<i class="fa fa-check"></i>&nbsp;&nbsp; Check All&nbsp;&nbsp;&nbsp;</button>
								@endif
							</th>
						</tr>
					</thead>
					@if($item->hasChildren())
					<tbody>
						@foreach($item->children() as $child)
							@php
								$perms = \App\Models\Auths\Permission::where('name', 'like', $child->perms.'%')->get();
							@endphp
							<tr>
								<td style="padding-left: 33px;">{{ $child->title}}</td>
								<td class="text-center">
									@if($p = $perms->where('name', $child->perms.'-view')->first())
										<input type="checkbox" class="view check" name="check[]" value="{{ $p->name }}" @if($record->hasPermissionTo($child->perms.'-view')) checked @endif data-toggle="toggle" data-size="mini" data-on="Ya" data-off="Tidak" data-style="ios">
									@else
										<input type="checkbox" class="view check" name="check[]" value="{{ $child->perms.'-view' }}" data-toggle="toggle" data-size="mini" data-on="Ya" data-off="Tidak" data-style="ios">
									@endif
								</td>
								<td class="text-center">
									@if($p = $perms->where('name', $child->perms.'-add')->first())
										<input type="checkbox" class="create check" name="check[]" value="{{ $p->name }}" @if($record->hasPermissionTo($child->perms.'-add')) checked @endif data-toggle="toggle" data-size="mini" data-on="Ya" data-off="Tidak" data-style="ios">
									@else
										<input type="checkbox" class="create check" name="check[]" value="{{ $child->perms.'-add' }}" data-toggle="toggle" data-size="mini" data-on="Ya" data-off="Tidak" data-style="ios">
									@endif
								</td>
								<td class="text-center">
									@if($p = $perms->where('name', $child->perms.'-edit')->first())
										<input type="checkbox" class="update check" name="check[]" value="{{ $p->name }}" @if($record->hasPermissionTo($child->perms.'-edit')) checked @endif data-toggle="toggle" data-size="mini" data-on="Ya" data-off="Tidak" data-style="ios">
									@else
										<input type="checkbox" class="update check" name="check[]" value="{{ $child->perms.'-edit' }}" data-toggle="toggle" data-size="mini" data-on="Ya" data-off="Tidak" data-style="ios">
									@endif
								</td>
								<td class="text-center">
									@if($p = $perms->where('name', $child->perms.'-delete')->first())
										<input type="checkbox" class="delete check" name="check[]" value="{{ $p->name }}" @if($record->hasPermissionTo($child->perms.'-delete')) checked @endif data-toggle="toggle" data-size="mini" data-on="Ya" data-off="Tidak" data-style="ios">
									@else
										<input type="checkbox" class="delete check" name="check[]" value="{{ $child->perms.'-delete' }}" data-toggle="toggle" data-size="mini" data-on="Ya" data-off="Tidak" data-style="ios">
									@endif
								</td>
								<td class="text-center">
									<button type="button" class="btn btn-xs btn-default horizontal all" style="border-radius: 20px;">&nbsp;&nbsp;<i class="fa fa-check"></i>&nbsp;&nbsp; Check All&nbsp;&nbsp;&nbsp;</button>
								</td>
							</tr>
						@endforeach
					</tbody>
					@endif
				@endforeach
			</table>
			<div class="panel-footer">
				<a href="{{ url()->previous() }}" class="btn btn-sm btn-default" style="border-radius: 20px;">
					<i class="fa fa-angle-left"></i>&nbsp;&nbsp; Kembali
				</a>
				<button type="button" class="btn btn-sm btn btn-simpan pull-right save button">
					<i class="fa fa-save"></i>&nbsp;&nbsp; Simpan
				</button>
			</div>
		</div>
	    <div class="loading dimmer padder-v" style="display: none">
	        <div class="loader"></div>
	    </div>
	</form>
@endsection

@push('scripts')
<script>
	$(document).on('click', '.vertical.all', function(e){
		e.preventDefault();
		var container 	= $(this).closest('thead');
		var action 		= $(this).data('action');
		var selector	= $('.'+action+'.check');
		var checked		= true;
		container.next('tbody').find(selector).each(function(e){
			checked = this.checked = !this.checked;
			// $(this).prop('checked', !$(this).prop('checked'));
		});
		container.next('tbody').find(selector).trigger('click');
	});

	$(document).on('click', '.horizontal.all', function(e){
		e.preventDefault();
		var container 	= $(this).closest('tr');
		var selector	= $('.check');
		var checked		= true;
		container.find(selector).each(function(e){
			checked = this.checked = !this.checked;
			// $(this).prop('checked', !$(this).prop('checked'));
		});

		container.find(selector).trigger('click');
	});

    $(document).on('click', '.save.button', function(e){
        saveData('formData', function(resp){
            location.href = "{{ route($routes.'.index') }}";
        });
    });
</script>
@endpush