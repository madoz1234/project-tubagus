<aside id="aside" class="app-aside">
   {{--  <a href="#/" class="navbar-brand text-lt">
        <img src="{{ asset('favicon-pragma.ico') }}" alt="." class="avatar"> 
    </a> --}}
    <div class="aside-wrap" style="background-image: url('../img/greyzz.png')">
        <div class="aside-wrap" id="style-7">
            <!-- nav -->
            <nav ui-nav="" class="navi clearfix">
                <ul class="nav">
                    @include('partials.menu', ['items' => $sideMenu->roots()])
                </ul>
            </nav>
        </div>
        <!-- / user -->
    </div>
</aside>