<form action="{{ route($routes.'.store') }}" method="POST" id="formData">
    @csrf
    <div class="modal-header">
        <h5 class="modal-title" style="font-weight: bold;">Tambah Data Nilai Alternatif</h5>
    </div>
    <div class="modal-body">
        <div class="form-group field">
            <label class="control-label">Nama Alternatif</label>
            <select class="selectpicker form-control show-tick" data-size="3" name="alternatif_id" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
                @foreach($alternatif as $alternatif)
                    <option value="{{ $alternatif->id }}">{{ $alternatif->nama }}</option>
                @endforeach
            </select>
        </div>
        @foreach($bantuan as $bantuans)
            <div class="form-group field">
                <label class="control-label">{{ $bantuans->nama }}</label>
                <input type="text" name="detail[{{ $bantuans->id }}][nilai]" class="form-control" placeholder="Nilai {{ $bantuans->nama }}" required="" value="">
            </div>
        @endforeach
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Kembali</button>
        <button type="button" class="btn btn-simpan save button">Simpan</button>
    </div>

    <div class="loading dimmer padder-v">
        <div class="loader"></div>
    </div>
</form>