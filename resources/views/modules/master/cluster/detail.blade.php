@extends('layouts.base')

@push('css')
<link rel="stylesheet" href="#" type="text/css" />
<link rel="stylesheet" href="{{ asset('libs/assets/one-calender/simple-calendar.css') }}" type="text/css" />
@endpush

@push('js')
<script src="{{ asset('libs/assets/one-calender/jquery.simple-calendar.js') }}"></script>
<script src="http://malsup.github.com/jquery.form.js"></script> 
{{-- <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
<script src="https://apexcharts.com/samples/assets/stock-prices.js') }}"></script> --}}
@endpush

@section('title', 'Detil Cluster')

@section('side-header')
<div style="margin-right: 14px;">
  <span><i class="glyphicon glyphicon-home"></i></span>
</div>
@endsection

@push('styles')
@endpush

@push('scripts')
@endpush

@section('body')
<div class="row row-sm">
  <div class="panel b-a">
    <div class="panel-heading text-left no-border">
      <span class="h4 m-b font-thin">Detail Cluster {{ $record->nama }}</span>          
    </div>
    <div class="panel-body" style="padding-bottom: 0px;">
      <form action="{{ route($routes.'.simpan', $record->id) }}" method="POST" id="formData">
        @method('PATCH')
        @csrf
        <input type="hidden" name="id" value="{{ $record->id }}">
        <div class="loading dimmer padder-v" style="display: none;">
            <div class="loader"></div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-12">
                <table id="example" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
                    <thead style="background-color: #f9fafb;">
                        <tr>
                            <th scope="col" style="text-align: center;width: 50px;">No</th>
                            @foreach($bantuan as $data)
                            <th scope="col" style="text-align: center;">{{ $data->nama }}</th>
                            @endforeach
                        </tr>
                    </thead>
                    <tbody class="container">
                        <tr class="data-container-1" data-id="1">
                            <td scope="row" style="text-align: center;">
                                <label style="margin-top: 23px;" class="numboor-1">1</label>
                            </td>
                            @foreach($bantuan as $datas)
                            @php 
                                $cek = App\Models\Master\ClusterDetail::where('clusterid', $record->id)->where('bantuanid', $datas->id)->first();
                            @endphp
                            <td scope="row" style="text-align: center;" class="field">
                                @if(is_null($cek))
                                <input type="text" name="detail[{{ $datas->id }}][nilai]" style="margin-top: 16px;" class="form-control" placeholder="Nilai" value="">
                                @else 
                                <input type="hidden" name="detail[{{ $datas->id }}][id]" style="margin-top: 16px;" value="{{ $cek->id }}">
                                <input type="text" name="detail[{{ $datas->id }}][nilai]" style="margin-top: 16px;" class="form-control" placeholder="Nilai" value="{{ $cek->nilai }}">
                                @endif
                            </td>
                            @endforeach
                       </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <p>&nbsp;</p>
        <div class="form-row">
          <div class="form-group col-md-12">
            <div class="text-right">
              <button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
              <button type="button" class="btn btn-simpan save as page">Simpan</button>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
@push('scripts')
    <script>
      $(document).on('click', '.save.as.page', function(e){
        $('#formData').find('input[name="status"]').val("1");
        var formDom = "formData";
        if($(this).data("form") !== undefined){
          formDom = $(this).data('form');
        }
        saveForm(formDom);
      });

      function saveForm(form="formData")
      {
        $('#' + form).find('.loading.dimmer').show();
        $("#"+form).ajaxSubmit({
          success: function(resp){
              var url = "{!! route($routes.'.index') !!}";
                window.location = url;
                return true;
            },
          error: function(resp){
            $('#' + form).find('.loading.dimmer').hide();
            if (resp.responseJSON.status !== "undefined" && resp.responseJSON.status === 'false'){
              swal(
                'Oops, Maaf!',
                resp.responseJSON.message,
                'error'
                )
            }

            $('#cover').hide();
            var response = resp.responseJSON;
            $.each(response.errors, function(index, val) {
              clearFormError(index,val);
              showFormError(index,val);
            });
            time = 5;
            interval = setInterval(function(){
              time--;
              if(time == 0){
                clearInterval(interval);
                $('.pointing.prompt.label.transition.visible').remove();
                $('.field .error-label').slideUp(500, function(e) {
                  $(this).remove();
                  $('.field.has-error').removeClass('has-error');
                  clearTimeout(interval);
                });
              }
            },1000)
          }
        });
      }

        showFormError = function(key, value)
        {
            if(key.includes("."))
            {
                res = key.split('.');
                key = res[0] + '[' + res[1] + ']';
                if(res[1] == 0)
                {
                    key = res[0] + '\\[\\]';
                    var exist = $('#formData' + ' [name="' + res[0] + '[' + res[1] + ']' + '"]');
                    if(exist.length > 0)
                    {
                        key = res[0] + '[' + res[1] + ']';
                    }
                }
                if(res[2])
                {
                    key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']';
                    if(res[2] == 0)
                    {
                        key = res[0] + '['+ res[1] +']' + '\\[\\]';
                    }
                }
                if(res[3])
                {
                    key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']' + '[' + res[3] + ']';
                    if(res[3] == 0)
                    {
                        key = res[0] + '['+ res[1] +']' + '['+ res[2] +']' + '\\[\\]';
                    }
                }
                if(res[4])
                {
                    key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']' + '[' + res[3] + ']' + '[' + res[4] + ']';
                    if(res[4] == 0)
                    {
                        key = res[0] + '['+ res[1] +']' + '['+ res[2] +']' + '['+ res[3] +']' + '\\[\\]';
                    }
                }
                if(res[5])
                {
                    key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']' + '[' + res[3] + ']' + '[' + res[4] + ']' + '[' + res[5] + ']';
                    if(res[5] == 0)
                    {
                        key = res[0] + '['+ res[1] +']' + '['+ res[2] +']' + '['+ res[3] +']' + '['+ res[4] +']' + '\\[\\]';
                    }
                }
                if(res[6])
                {
                    key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']' + '[' + res[3] + ']' + '[' + res[4] + ']' + '[' + res[5] + ']' + '[' + res[6] + ']';
                    if(res[6] == 0)
                    {
                        key = res[0] + '['+ res[1] +']' + '['+ res[2] +']' + '['+ res[3] +']' + '['+ res[4] +']' + '['+ res[5] +']' + '\\[\\]';
                    }
                }
            }
            var elm = $('[name^="'+ key +'"]').closest('.field');
            var tabs = $('[name^="'+ key +'"]').parents('.tab.segment');
            if(tabs.length > 0)
            {
                selectedTabs(tabs);
            }

            // var fg = $('[name="'+ name +'"], [name="'+ name +'[]"]').closest('.form-group');
            // fg.addClass('has-error');
            // fg.append('<small class="control-label error-label font-bold">'+ val +'</small>')

            var message = `<small class="control-label error-label font-bold">`+ value +`</small>`;
            var showerror = $('[name^="'+ key +'"]').closest('.field');
            var multipleCheckbox = $(showerror).parents('.multiple-checkbox');
            if($('[name^="'+ key +'"]').closest('.field').find('input').length > 0 && $('[name^="'+ key +'"]').closest('.field').find('input').hasClass('hidden'))
            {
                $(elm).addClass('has-error');
                if($('[name^="'+ key +'"]').closest('.field').find('div').length > 0)
                {
                    $('[name^="'+ key +'"]').closest('.field').find('div').append('<small class="control-label error-label font-bold">' + value + '</small>');
                }else{
                    $(showerror).append('<small class="control-label error-label font-bold">' + value + '</small>');
                }
            }else{
                if(multipleCheckbox.length > 0)
                {
                    multipleCheckboxLabel = multipleCheckbox.find('label:first-child');
                    $(multipleCheckboxLabel).append('<span class="red error-label" style="color:#9f3a38 !important;">' + value + '</span>');
                }else{
                    $(elm).addClass('has-error');
                    $(showerror).append('<small class="control-label error-label font-bold">' + value + '</small>');
                }
            }
        }

        clearFormError = function(key, value)
        {
            if(key.includes("."))
            {
                res = key.split('.');
                key = res[0] + '[' + res[1] + ']';
                if(res[1] == 0)
                {
                    key = res[0] + '\\[\\]';
                    var exist = $(' [name="' + res[0] + '[' + res[1] + ']' + '"]');
                    if(exist.length > 0)
                    {
                        key = res[0] + '[' + res[1] + ']';
                    }
                }
                if(res[2])
                {
                    key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']';
                    if(res[2] == 0)
                    {
                        key = res[0] + '['+ res[1] +']' + '\\[\\]';
                    }
                }
                if(res[3])
                {
                    key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']' + '[' + res[3] + ']';
                    if(res[3] == 0)
                    {
                        key = res[0] + '['+ res[1] +']' + '['+ res[2] +']' + '\\[\\]';
                    }
                }
                if(res[4])
                {
                    key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']' + '[' + res[3] + ']' + '[' + res[4] + ']';
                    if(res[4] == 0)
                    {
                        key = res[0] + '['+ res[1] +']' + '['+ res[2] +']' + '['+ res[3] +']' + '\\[\\]';
                    }
                }
                if(res[5])
                {
                    key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']' + '[' + res[3] + ']' + '[' + res[4] + ']' + '[' + res[5] + ']';
                    if(res[5] == 0)
                    {
                        key = res[0] + '['+ res[1] +']' + '['+ res[2] +']' + '['+ res[3] +']' + '['+ res[4] +']' + '\\[\\]';
                    }
                }

                if(res[6])
                {
                    key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']' + '[' + res[3] + ']' + '[' + res[4] + ']' + '[' + res[5] + ']' + '[' + res[6] + ']';
                    if(res[6] == 0)
                    {
                        key = res[0] + '['+ res[1] +']' + '['+ res[2] +']' + '['+ res[3] +']' + '['+ res[4] +']' + '['+ res[5] +']' + '\\[\\]';
                    }
                }
            }

            if($('[name^="'+ key +'"]').closest('.field').find('div').length > 0){
                var elm = $('[name^="'+ key +'"]').closest('.field');
                $(elm).removeClass('has-error');

                var showerror = $('[name^="'+ key +'"]').closest('.field').find('.control-label.error-label.font-bold').remove();
            }else{
                var elm = $('[name^="'+ key +'"]').closest('.field');
                $(elm).removeClass('has-error');

                var showerror = $('[name^="'+ key +'"]').closest('.field').find('.control-label.error-label.font-bold').remove();
            }
        }

    </script>
    @yield('js-extra')
@endpush