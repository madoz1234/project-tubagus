@extends('layouts.app')

@push('css')
    <link rel="stylesheet" href="{{ asset('css/form.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('libs/assets/bootstrap-select/bootstrap-select.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('libs/assets/apexcharts/dist/apexcharts.css') }}" type="text/css" />
@endpush

@push('js')
    <script src="{{ asset('libs/assets/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('libs/jquery/redirect/jquery.redirect.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="{{ asset('libs/assets/apexcharts/dist/apexcharts.js') }}"></script>
@endpush


@section('content')
<div id="content" class="app-content app-content-full fade-in h-full" role="main">
    <div class="app-content-body">
        <div class="hbox hbox-auto-xs hbox-auto-sm" ng-init="
            app.settings.asideFolded = false;
            app.settings.asideDock = false;
            ">
            <!-- main -->
            <div class="col">
                @section('main')
                <!-- main header -->
                <div class="wrapper-md">
                    <div class="col-sm-6 col-xs-12">
                        <a href="javascript:void(0)" class="h3 pull-right visible-xs" data-action="menu-toggle"><i class="fa fa-bars"></i></a>
                        <h1 class="m-n font-thin h4 text-black" style="font-weight: bold;">@yield('title', 'TITLE') @if(trim($__env->yieldContent('subtitle')))&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<small>@yield('subtitle', 'Subtitle here')</small>@endif</h1>
                    </div>
                    <div class="col-sm-6 text-right hidden-xs" style="right: -14px;">
                        @section('side-header')
                        <nav aria-label="breadcrumb">
                        	<ol class="breadcrumb">
                        		<li class="breadcrumb-item"><a href="#">Home</a></li>
                        		<li class="breadcrumb-item"><a href="#">Library</a></li>
                        		<li class="breadcrumb-item active" aria-current="page">Data</li>
                        	</ol>
                        </nav>
                        @show
                    </div>
                </div>
                <!-- / main header -->
                <div class="wrapper-md">
                  @yield('body')
                </div>
                @show
            </div>
            <!-- / main -->
        </div>
    </div>
</div>
@endsection

@push('modals')
    <!-- Large modal -->
    <div id="largeModal" class="modal fade form-modal-lg" tabindex="-1" role="dialog" aria-labelledby="largeModal">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="loading dimmer padder-v">
                    <div class="loader"></div>
                </div>
            </div>
        </div>
    </div>

    <!-- Standar modal -->
    <div id="mediumModal" class="modal fade form-modal-md" tabindex="-1" role="dialog" aria-labelledby="mediumModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="loading dimmer padder-v">
                    <div class="loader"></div>
                </div>
            </div>
        </div>
    </div>

    <!-- Small modal -->
    <div id="smallModal" class="modal fade form-modal-sm" tabindex="-1" role="dialog" aria-labelledby="smallModal">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="loading dimmer padder-v">
                    <div class="loader"></div>
                </div>
            </div>
        </div>
    </div>
@endpush