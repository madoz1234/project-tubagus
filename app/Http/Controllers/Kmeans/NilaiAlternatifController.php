<?php

namespace App\Http\Controllers\Kmeans;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

use App\Http\Requests\Kmeans\NilaiAlternatifRequest;
use App\Models\Master\Alternatif;
use App\Models\Master\Bantuan;
use App\Models\Kmeans\NilaiAlternatif;

use DB;

class NilaiAlternatifController extends Controller
{
    protected $routes = 'kmeans.nilai';

    public function __construct()
    {
        $this->setRoutes($this->routes);
        $this->setBreadcrumb(['Kmeans' => '#', 'Nilai Alternatif' => '#']);
        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => 'text-center',
                'width' => '20px',
            ],
            /* --------------------------- */
            [
                'data' => 'nama',
                'name' => 'nama',
                'label' => 'Nama Alternatif',
                'className' => 'text-center',
                'width' => '300px',
                'sortable' => true,
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Dibuat Pada',
                'className' => 'text-center',
                'sortable' => true,
                'width' => '120px',
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'width' => '80px',
                'className' => 'text-center'
            ]
        ]);
    }

    public function grid()
    {
        $records = NilaiAlternatif::select('*');
        if(!isset(request()->order[0]['column'])) {
              $records->orderBy('created_at');
        }
        if ($name = request()->name) {
            $records->where('nama', 'like', '%' . $name . '%');
        }
        return DataTables::of($records->get())
               ->addColumn('num', function ($record) {
                   return request()->start;
               })
               ->addColumn('nama', function ($record) {
                   return $record->alternatif->nama;
               })
               ->editColumn('created_at', function ($record) {
                   return $record->created_at->diffForHumans();
               })
               ->addColumn('action', function ($record) {
                   $buttons = '';
                   $buttons .= $this->makeButtons([
                        'type'      => 'edit',
                        'label'     => '<i class="fa fa-eye text-primary"></i>',
                        'tooltip'   => 'Detail',
                        'class'     => 'detail button',
                        'id'        => $record->id,
                    ]);
                   $buttons .='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                   $buttons .= $this->makeButton([
                        'type' => 'edit',
                        'id'   => $record->id,
                   ]);
                   return $buttons;
               })
               ->rawColumns(['status', 'action'])
               ->make(true);
    }

    public function index()
    {
        return $this->render('modules.kmeans.nilai-alternatif.index');
    }

    public function create()
    {
        $bantuan = Bantuan::get();
        $alternatif = Alternatif::doesntHave('nilaialternatif')->get();
        return $this->render('modules.kmeans.nilai-alternatif.create', [
            'bantuan' => $bantuan,
            'alternatif' => $alternatif
        ]);
    }

    public function store(NilaiAlternatifRequest $request)
    {
    	DB::beginTransaction();
    	try {
    		$record = new NilaiAlternatif;
			$record->alternatif_id 	= $request->alternatif_id;
	        $record->save();
            $record->saveDetail($request->detail);

	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function edit(NilaiAlternatif $nilai)
    {
        $bantuan = Bantuan::get();
        return $this->render('modules.kmeans.nilai-alternatif.edit', [
            'record' => $nilai,
            'bantuan' => $bantuan
        ]);
    }

    public function detail(NilaiAlternatif $id)
    {
       $bantuan = Bantuan::get();
        return $this->render('modules.kmeans.nilai-alternatif.detail', [
            'record' => $id,
            'bantuan' => $bantuan
        ]);
    }

    public function update(NilaiAlternatifRequest $request, NilaiAlternatif $nilai)
    {
    	DB::beginTransaction();
        try {
        	$record = NilaiAlternatif::find($request->id);
			$record->alternatif_id 		= $request->alternatif_id;
	        $record->updateDetail($request->detail);

	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function destroy(Alternatif $alternatif)
    {
        if($alternatif){
    		
    	}else{
    		return response([
                'status' => true,
            ],500);
    	}
    }
}
