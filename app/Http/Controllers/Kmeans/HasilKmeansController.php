<?php

namespace App\Http\Controllers\Kmeans;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Libraries\CoreSn;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Master\Alternatif;
use App\Models\Master\Bantuan;
use App\Models\Master\Cluster;
use App\Models\Kmeans\NilaiAlternatif;
use App\Models\Ahp\HasilAHP;
use App\Models\Kmeans\NilaiAlternatifDetail;
use App\Models\Kmeans\Kmeans;
use App\Models\Kmeans\KmeansDetail;
use Carbon\Carbon;

class HasilKmeansController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $routes = 'kmeans.hasil';
    public function __construct()
    {
        $this->setRoutes($this->routes);
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $nilai = NilaiAlternatif::get();
        $bantuan = Bantuan::get();
        $alternatif = Alternatif::get();
        $nilaidetail = NilaiAlternatifDetail::get();
        $ahp = HasilAHP::get();
        $cluster = Cluster::get();
        $hasil =0;
        foreach($ahp as $ahps){
         foreach($nilai->where('alternatif_id', $ahps->alternatif->id) as $alternatifs){
          foreach($cluster as $clusters){
            if(!is_null($clusters->detail)){
              foreach($clusters->detail as $details){
                  $nilai_detail = $alternatifs->detail->where('bantuan_id', $details->bantuanid)->first()->nilai;
                  $jum = pow($nilai_detail - $details->nilai, 2);
                  $hasil +=$jum;
              }
              $kmeans = Kmeans::where('alternatifid', $ahps->alternatif->id)->first();
              if(is_null($kmeans)){
                  $new = new Kmeans;
                  $new->alternatifid = $ahps->alternatif->id;
                  $new->save();
                  $cekz = KmeansDetail::where('kmeansid', $new->id)->where('cluster_detail_id', $details->id)->first();
                  if(is_null($cekz)){
                    $newdetail = new KmeansDetail;
                    $newdetail->kmeansid = $new->id;
                    $newdetail->cluster_detail_id = $details->id;
                    $newdetail->nilai = sqrt(round($hasil));
                    $newdetail->save();
                  }else{
                    $newdetail = KmeansDetail::find($cekz->id);
                    $newdetail->kmeansid = $new->id;
                    $newdetail->cluster_detail_id = $details->id;
                    $newdetail->nilai = sqrt(round($hasil));
                    $newdetail->save();
                  }
              }else{
                  $new = Kmeans::find($kmeans->id);
                  $new->alternatifid = $ahps->alternatif->id;
                  $new->save();
                  if(!is_null($details)){
                    $cekz = KmeansDetail::where('kmeansid', $kmeans->id)->where('cluster_detail_id', $details->id)->first();
                    if(is_null($cekz)){
                      $newdetail = new KmeansDetail;
                      $newdetail->kmeansid = $kmeans->id;
                      $newdetail->cluster_detail_id = $details->id;
                      $newdetail->nilai = sqrt(round($hasil));
                      $newdetail->save();
                    }else{
                      $newdetail = KmeansDetail::find($cekz->id);
                      $newdetail->kmeansid = $kmeans->id;
                      $newdetail->cluster_detail_id = $details->id;
                      $newdetail->nilai = sqrt(round($hasil));
                      $newdetail->save();
                    }
                  }
              }
            }
          }
         }
        }
        return $this->render('modules.kmeans.hasil', [
            'mockup' => true,
            'bantuan' => $bantuan,
            'alternatif' => $alternatif,
            'nilai' => $nilai,
            'nilaidetail' => $nilaidetail,
            'cluster' => $cluster,
            'ahp' => $ahp,
        ]);
    }
}
