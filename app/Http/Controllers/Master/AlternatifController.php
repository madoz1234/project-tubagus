<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

use App\Http\Requests\Master\AlternatifRequest;
use App\Models\Master\Alternatif;
use App\Models\SessionLog;

use DB;

class AlternatifController extends Controller
{
    protected $routes = 'master.alternatif';

    public function __construct()
    {
        $this->setRoutes($this->routes);
        $this->setBreadcrumb(['Master' => '#', 'Alternatif' => '#']);
        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => 'text-center',
                'width' => '20px',
            ],
            /* --------------------------- */
            [
                'data' => 'nama',
                'name' => 'nama',
                'label' => 'Nama Alternatif',
                'className' => 'text-center',
                'width' => '300px',
                'sortable' => true,
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Dibuat Pada',
                'className' => 'text-center',
                'sortable' => true,
                'width' => '120px',
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'width' => '80px',
                'className' => 'text-center'
            ]
        ]);
    }

    public function grid()
    {
        $records = Alternatif::select('*');
        if(!isset(request()->order[0]['column'])) {
              $records->orderBy('created_at');
        }
        if ($name = request()->name) {
            $records->where('nama', 'like', '%' . $name . '%');
        }
        return DataTables::of($records->get())
               ->addColumn('num', function ($record) {
                   return request()->start;
               })
               ->addColumn('nama', function ($record) {
                   return $record->nama;
               })
               ->editColumn('created_at', function ($record) {
                   return $record->created_at->diffForHumans();
               })
               ->addColumn('action', function ($record) {
                   $buttons = '';

                   $buttons .= $this->makeButton([
                        'type' => 'edit',
                        'id'   => $record->id,
                   ]);
                   return $buttons;
               })
               ->rawColumns(['status', 'action'])
               ->make(true);
    }

    public function index()
    {
        return $this->render('modules.master.alternatif.index');
    }

    public function create()
    {
        return $this->render('modules.master.alternatif.create');
    }

    public function store(AlternatifRequest $request)
    {
    	DB::beginTransaction();
    	try {
    		$record = new Alternatif;
			$record->nama 					= $request->nama;
	        $record->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function edit(Alternatif $alternatif)
    {
        return $this->render('modules.master.alternatif.edit', ['record' => $alternatif]);
    }

    public function update(AlternatifRequest $request, Alternatif $alternatif)
    {
    	DB::beginTransaction();
        try {
        	$record = Alternatif::find($request->id);
			$record->nama 					= $request->nama;
	        $record->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function destroy(Alternatif $alternatif)
    {
        if($alternatif){
    		
    	}else{
    		return response([
                'status' => true,
            ],500);
    	}
    }
}
