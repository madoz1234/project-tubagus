<?php

namespace App\Http\Requests\Master;

use App\Http\Requests\FormRequest;

class ClusterDetailRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	$return = [
            'detail.*.nilai'            			=> 'required',
        ];
		return $return;
    }

    public function messages()
    {
    	return [
        	'detail.*.nilai.required'            		=> 'Nilai tidak boleh kosong',
       ];
    }
}
