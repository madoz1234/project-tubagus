<?php

namespace App\Http\Requests\Master;

use App\Http\Requests\FormRequest;

class ClusterRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	$return = [
            'nama'            			=> 'required|max:200|unique:ref_cluster,nama,'.$this->get('id'),
        ];
		return $return;
    }

    public function messages()
    {
    	return [
        	'nama.required'            		=> 'Nama Cluster tidak boleh kosong',
        	'nama.unique'            		=> 'Nama Cluster sudah ada',
       ];
    }
}
