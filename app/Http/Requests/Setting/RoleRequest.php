<?php

namespace App\Http\Requests\Setting;

use App\Http\Requests\FormRequest;

class RoleRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	$return = [
            'name'            			=> 'required|unique:sys_roles,name,'.$this->get('id'),
        ];
		return $return;
    }

    public function messages()
    {
    	return [
        	'name.required'            		=> 'Role tidak boleh kosong',
        	'name.unique'            		=> 'Role sudah ada',
       ];
    }
}
