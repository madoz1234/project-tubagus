<?php

namespace App\Http\Requests\Master;

use App\Http\Requests\FormRequest;

class KriteriaRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	$return = [
            'kode'            			=> 'required|max:200|unique:ref_kriteria,kode,'.$this->get('id'),
            'nama'            			=> 'required|max:200|unique:ref_kriteria,nama,'.$this->get('id'),
            'bobot'            			=> 'required',
        ];
		return $return;
    }

    public function messages()
    {
    	return [
        	'nama.required'            		=> 'Nama Kriteria tidak boleh kosong',
        	'kode.required'            		=> 'Kode tidak boleh kosong',
        	'bobot.required'            	=> 'Bobot tidak boleh kosong',
        	'nama.unique'            		=> 'Nama Kriteria sudah ada',
        	'kode.unique'            		=> 'Kode sudah ada',
       ];
    }
}
