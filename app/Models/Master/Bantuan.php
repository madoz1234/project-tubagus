<?php

namespace App\Models\Master;

use App\Models\Model;

class Bantuan extends Model
{
    /* default */
    protected $table 		= 'ref_bantuan';
    protected $fillable 	= ['nama'];

    /* data ke log */
    // protected $log_table    = 'log_ref_bantuan';
    // protected $log_table_fk = 'ref_id';
    /* relation */
    /* mutator */
    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
