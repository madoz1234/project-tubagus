<?php

namespace App\Models\Master;

use App\Models\Model;

class Cluster extends Model
{
    /* default */
    protected $table 		= 'ref_cluster';
    protected $fillable 	= ['nama'];

    /* data ke log */
    // protected $log_table    = 'log_ref_bantuan';
    // protected $log_table_fk = 'ref_id';
    /* relation */
    public function detail(){
        return $this->hasMany(ClusterDetail::class, 'clusterid' , 'id');
    }
    /* mutator */
    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
