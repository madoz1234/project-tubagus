<?php

namespace App\Models;

use App\Models\Model;

class Files extends Model
{
    protected $table 		= 'sys_files';
    protected $dates 	= ['taken_at'];

    protected $fillable 	= [
        'filename',
        'url',
        'target_type',
        'target_id',
        'type',
        'taken_at',
        'dms_update',
        'flag'
    ];
    protected $appends = [
        'attachment_url',
    ];

    public function target()
    {
        return $this->morphTo();
    }
    public function getAttachmentUrlAttribute()
    {
        return url('storage/'.$this->url);
    }
}
