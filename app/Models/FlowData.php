<?php
namespace App\Models;

use App\Models\Model;

class FlowData extends Model
{
    /* default */
    protected $table 		= 'trans_flow_data';
    protected $fillable 	= ['data_id','pengirim_id','penerima_id', 'tipe','keterangan'];

    /* data ke log */
    protected $log_table    = 'log_trans_flow_data';
    protected $log_table_fk = 'ref_id';
    /* relation */
    // insert code here
    
    /* mutator */
    // insert code here


    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
