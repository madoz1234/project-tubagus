<?php

namespace App\Models\Kmeans;

use App\Models\Model;
use App\Models\Kmeans\KmeansDetail;

class Kmeans extends Model
{
    /* default */
    protected $table 		= 'trans_nilai_kmeans';
    protected $fillable 	= ['alternatif_id'];

    /* data ke log */
    // protected $log_table    = 'log_trans_nilai_alternatif_kmeans';
    // protected $log_table_fk = 'ref_id';
    /* relation */

    public function detail(){
        return $this->hasMany(KmeansDetail::class, 'kmeansid' , 'id');
    }

    public function saveDetail($detail)
    {   
        if($detail){
            foreach ($detail as $key => $value) {
                $nilaidetail = new KmeansDetail;
                $nilaidetail->kmeansid = $key;
                $nilaidetail->nilai = $value['nilai'];
                $this->detail()->save($nilaidetail);
            }
        }
    }

    public function updateDetail($detail)
    {   
        if($detail){
            $cari = KmeansDetail::where('kmeansid', $this->id)->delete();
            foreach ($detail as $key => $value) {
                $news = new KmeansDetail;
                $news->kriteria_id = $key;
                $news->nilai = $value['nilai'];
                $this->detail()->save($news);
            }
        }
    }
    /* mutator */
    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
