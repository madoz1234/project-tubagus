<?php

namespace App\Models\Kmeans;

use App\Models\Model;
use App\Models\Kmeans\NilaiAlternatif;
use App\Models\Master\Bantuan;

class NilaiAlternatifDetail extends Model
{
    /* default */
    protected $table 		= 'trans_nilai_alternatif_kmeans_detail';
    protected $fillable 	= ['nilai_id','bantuan_id','nilai'];

    /* data ke log */
    protected $log_table    = 'log_trans_nilai_alternatif_kmeans_detail';
    protected $log_table_fk = 'ref_id';
    /* relation */

    public function nilaialternatif(){
        return $this->belongsTo(NilaiAlternatif::class, 'nilai_id' , 'id');
    }

    public function bantuan(){
        return $this->belongsTo(Bantuan::class, 'bantuan_id' , 'id');
    }
    /* mutator */
    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
