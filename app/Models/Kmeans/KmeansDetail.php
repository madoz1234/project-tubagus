<?php

namespace App\Models\Kmeans;

use App\Models\Model;
use App\Models\Kmeans\Kmeans;
use App\Models\Master\ClusterDetail;

class KmeansDetail extends Model
{
    /* default */
    protected $table 		= 'trans_nilai_kmeans_detail';
    protected $fillable 	= ['kmeansid','cluster_detail_id','nilai'];

    /* data ke log */
    // protected $log_table    = 'log_trans_nilai_alternatif_kmeans';
    // protected $log_table_fk = 'ref_id';
    /* relation */
    public function kmeans(){
        return $this->belongsTo(Kmeans::class, 'kmeansid' , 'id');
    }

    public function clusterdetail(){
        return $this->belongsTo(ClusterDetail::class, 'cluster_detail_id' , 'id');
    }
    /* mutator */
    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
