<?php

namespace App\Models\Ahp;

use App\Models\Model;
use App\Models\Master\Alternatif;

class HasilAHP extends Model
{
    /* default */
    protected $table 		= 'trans_hasil_ahp';
    protected $fillable 	= ['alternatifid','nilai'];

    /* data ke log */
    // protected $log_table    = 'log_trans_hasil_ahp';
    // protected $log_table_fk = 'ref_id';
    /* relation */
    
    public function alternatif(){
        return $this->belongsTo(Alternatif::class, 'alternatifid' , 'id');
    }

    /* mutator */
    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
