<?php

namespace App\Models\Topsis;

use App\Models\Model;
use App\Models\Topsis\NilaiAlternatif;
use App\Models\Master\Kriteria;

class NilaiAlternatifDetail extends Model
{
    /* default */
    protected $table 		= 'trans_nilai_alternatif_topsis_detail';
    protected $fillable 	= ['nilai_id','kriteria_id','nilai'];

    /* data ke log */
    protected $log_table    = 'log_trans_nilai_alternatif_topsis_detail';
    protected $log_table_fk = 'ref_id';
    /* relation */

    public function nilaialternatif(){
        return $this->belongsTo(NilaiAlternatif::class, 'nilai_id' , 'id');
    }

    public function kriteria(){
        return $this->belongsTo(Kriteria::class, 'kriteria_id' , 'id');
    }
    /* mutator */
    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
