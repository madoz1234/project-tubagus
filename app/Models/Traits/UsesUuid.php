<?php

namespace App\Models\Traits;

use Illuminate\Support\Str;

trait UsesUuid
{
    public static function bootUsesUuid()
    {
        static::creating(function ($model) {
            $model->{$model->getKeyName()} = (string) Str::uuid();
        });
    }

    public function getIncrementing()
    {
        return false;
    }

    public function getKeyType()
    {
        return 'string';
    }
}
