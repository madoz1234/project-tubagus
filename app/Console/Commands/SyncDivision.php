<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use App\Models\Dms;
use App\Models\Files;
use App\Models\Master\BU;
use App\Models\Master\CO;

use Illuminate\Filesystem\Filesystem;
use Storage;
use Carbon\Carbon;

class SyncDivision extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:division';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Division';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $client = new Client;
      $response = $client->request('GET', 'https://west.waskita.co.id/page/tlcc/apiwest/apiwest.php?secret='.$this->login().'&group=obj_parent&obj_level="DIVISION"');

      $divisions = json_decode($response->getBody()->getContents());

      if(count($divisions) > 0)
      {
          foreach($divisions as $division)
          {
            try {
              if($this->checkDivision($division->short_text) == 1)
              {
                $bu = BU::where('nama', 'like', '%'.$division->short_text.'%')->first();
                if(!$bu)
                {
                    $bu = new BU;
                }
                $bu->obj_id = $division->obj_id;
                $bu->kode = $division->short_text;
                $bu->nama = $division->short_text;
                $bu->save();

                $this->info('Business Unit '. $division->short_text .' TELAH DISIMPAN');
              }else{
                $co = CO::where('nama', 'like', '%'.$division->short_text.'%')->first();
                if(!$co)
                {
                    $co = new CO;
                }

                $co->obj_id = $division->obj_id;
                $co->kode = $division->short_text;
                $co->nama = $division->short_text;
                $co->save();

                $this->info('Corporate Office '. $division->short_text .' TELAH DISIMPAN');
              }

            }catch (\Exception $exception){
              $this->info('DIVISI TIDAK DAPAT DISIMPAN');
            }
          }
      }

    }

    public function login()
    {
      $client = new Client;

      $response = $client->request('GET', 'https://west.waskita.co.id/page/tlcc/apiwest/login_mobile.php?username=app-welcome&password=hris&fcm_token');

      return json_decode($response->getBody()->getContents())->secret;

    }

    public function checkDivision($name)
    {
        switch($name)
        {
            case 'Legal' : return 2;
            break;
            case 'Finance Division' : return 2;
            break;
            case 'Accounting Division' : return 2;
            break;
            case 'HCM Division' : return 2;
            break;
            case 'IT Division' : return 2;
            break;
            case 'System, Technology & Research Division' : return 2;
            break;
            case 'Business Strategy Division' : return 2;
            break;
            case 'QHSE Division' : return 2;
            break;
            case 'Risk Management Division' : return 2;
            break;
            case 'Marketing Division' : return 2;
            break;
            case 'Production Control Division' : return 2;
            break;
            case 'Supply Chain Management Division' : return 2;
            break;
            case 'Building Division': return 1;
            break;
            case 'Infra I Division': return 1;
            break;
            case 'Infra II Division': return 1;
            break;
            case 'Infra III Division': return 1;
            break;
            case 'EPC Division': return 1;
            break;

            default: return 0;
        }
    }

}
